#!/bin/ash
set -e

envsubst <main.tmpl.cf >/etc/postfix/main.cf

exec "$@"
