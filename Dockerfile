FROM alpine
COPY entrypoint.sh .
COPY main.tmpl.cf .

RUN apk add postfix gettext && \
    rm -rf /var/cache/apk/*

ENTRYPOINT ["/entrypoint.sh"]
EXPOSE 25
CMD ["/usr/sbin/postfix","-c","/etc/postfix","start-fg"]

LABEL org.opencontainers.image.authors="Ettore Leandro Tognoli <ettoreleandrotognoli@gmail.com>" \
    org.opencontainers.image.url="https://gitlab.com/ettotog/postfix" \
    org.opencontainers.image.source="https://gitlab.com/ettotog/postfix" \
    org.opencontainers.image.documentation="https://gitlab.com/ettotog/postfix" \
    org.opencontainers.image.licenses="Apache-2.0" \
    org.opencontainers.image.title="POSTFIX" \
    org.opencontainers.image.description="POSTFIX SMTP Server"
